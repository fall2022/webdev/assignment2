/**
 * @author Fred-Dgennie Louis, 2036915
 */
import currency_json from '../json/currency.json' assert {type: 'json'};
'use strict'
window.addEventListener('DOMContentLoaded', init);

/**
 * function that initialize the options for the user
 * and set event listeners on all the button
 */
function init() {

    //buttons
    let convertBtn = document.getElementById('convertBtn');
    let showBtn = document.getElementById('showBtn');
    let clearBtn = document.getElementById('clearBtn');

    //iniatializing the select options
    initialize_Select_Options_data(currency_json);
    console.log(currency_json)
    
    //events
    convertBtn.addEventListener('click', convertMoney);
    showBtn.addEventListener('click', showHistory);
    clearBtn.addEventListener('click', clearHistory);
}

/**
 * class that for the creation of URL object
 */
class urlObject {
    constructor(_from, _to, _amount) {
        this.mainURL = `https://api.exchangerate.host/convert`;
        this.fromQry = `from=${_from}`;
        this.toQry = `to=${_to}`;
        this.amount = `amount=${_amount}`;
    }
    get absoluteURL () {
        //added places to round the number
        return `${this.mainURL}?${this.fromQry}&${this.toQry}&${this.amount}&places=2`;
    }
}

/**
 * @param {*} urlObj 
 * use fetch api to convert currency using the absoluteURL of an object
 * when requested data is resolved, it call displayresult()
 */
function getData(urlObj) {

    let url = urlObj.absoluteURL;

    try {
        fetch(url)
            .then(response => response.json())
            .then(data => displayResult(data));  
    } 
    catch (error) {
        alert('Something went wrong. Try again');
    }
}   

/**
 * @param {*} data 
 * once the data is fetched, display result of conversion using createTable()
 * it then save the conversion in the local storage
 */
function displayResult(data) {
    let rowData = [data.query.from, data.query.to, data.info.rate, data.query.amount, data.result, data.date];

    let convertObj = [];
    convertObj.push(rowData);
    createTable(convertObj);

    //saving the data to localStorage
    window.localStorage.setItem(`${data.query.from}&${data.query.to}&${data.query.amount}`, JSON.stringify(rowData));
}



/**
 * this function will check that every field is filled and display am a;ert if they're not
 * then it will create a urlObject based on the user input
 * then call getData()
 */
function convertMoney() {

    const fromCurrency = document.getElementById('base-currency-select');
    const toCurrency = document.getElementById('to-currency-select');
    const moneyAmount = document.getElementById('amount');

    if (fromCurrency.value === '-' || toCurrency.value === '-' || moneyAmount<= 0) {
        alert('You must fill all the input fields');

        if (fromCurrency.value === '-') {
            fromCurrency.setAttribute('required' , '');
        } else if (toCurrency.value === '-') {
            toCurrency.setAttribute('required' , '');
        } else if(moneyAmount <= 0) {
            moneyAmount.setAttribute('required' , '');
        }
    } 
    else {
        //removing required attribute
        fromCurrency.removeAttribute('required');
        toCurrency.removeAttribute('required');
        moneyAmount.removeAttribute('required');

        //creating the urlObject
        let convertUrl = new urlObject(fromCurrency.value, toCurrency.value, moneyAmount.value);
        getData(convertUrl);
    }
}