/**
 * @author Fred-Dgennie Louis, 2036915
 */
'use strict'

/**
 * async function that initialize the options of select by
 * calling currencyData() then
 * creating a loop and using createOptions() for each value
 */
function initialize_Select_Options_data(currency_json) {
    //let currency_options = await currencyData();
    let data = Object.values(currency_json);
    
    for (let i =0; i<data.length ; i++) {
        createOptions('base', data[i]);
        createOptions('to', data[i]);
    } 
}

/**
 * @param {*} typeC string to know if its a 'base' or 'to' currency
 * @param {*} currency object
 * creates an option element which value is the currency code 
 * and innerText is the currency name
 */
function createOptions(typeC, currency) {
    let select_el = document.getElementById(`${typeC}-currency-select`);
    let option_el = document.createElement('option');
    option_el.value = currency.code;
    option_el.innerText = currency.name;
    select_el.append(option_el);
}

/**
 * make a request to get the data from currency.json file
 * @returns the json data
 */
async function currencyData() {
    //let url = 'json/currency.json';
    let currency_data = await fetch(currency_json);
    return currency_data.json();
}

/**
 * function to create the table rows
 * @param cellType string to know if the cell is a th or td cell
 * @param cellValues array to fill each row
 * it then return a tr object
 */
function createRow(cellType, cellValues) { 
    let tr = document.createElement('tr');

        //loop to create each cell in table
        cellValues.forEach(element => {
            let cell = document.createElement(`${cellType}`);
            cell.innerText = element;
            //creating an id for the th Amount, Payment, Date to help with styling table
            if(cellType ==='th' && element === 'Amount' || element === 'Payment' || element === 'Date') {
                cell.setAttribute('id', `th_${element}`);
            }
            tr.append(cell);
        });
    return tr;
}

/**
 * 
 * @param {*} values array or object that will be used to create the whole table
 */
function createTable(values) {
    let table = document.getElementsByTagName('table')[0];
    table.replaceChildren();
   
    //creating header
    let headerTitle = ['From', 'To', 'Rate', 'Amount', 'Payment', 'Date'];
    let thead = document.createElement('thead');
    let th_row = createRow('th', headerTitle);
    thead.append(th_row);
    table.append(thead);

    //creating body
    let tbody = document.createElement('tbody');
    for(let i= 0; i<values.length; i++) {
        
        let tb_row = createRow('td', values[i]);
        tbody.append(tb_row);
    }
    table.append(tbody);
}

/**
 * this function will check if the storage is empty 
 * if it is, will display an alert
 * if it isnt will get the values and parse them
 * then use createTable to display the old conversions
 */
function showHistory() {

    if (window.localStorage.length <= 0) {
        alert('Nothing to show! Your history is empty!');
    } 
    else {
        let c_values = Object.values(window.localStorage);
        let arr = c_values.map(item => JSON.parse(item));
        createTable(arr);}
}

/**
 * this remove all the values in the storage 
 * and also remove the table
 */
function clearHistory() {
    window.localStorage.clear();
    
    let table = document.getElementsByTagName('table')[0];
    table.replaceChildren();
    setTimeout(() => {alert('Your history has been cleared!');} , 300);
    
}